package time;

import static org.junit.Assert.*;

import org.junit.Test;

public class TimeTest {

//	@Test
//	public void testGetTotalSecondsRegular() {
//		int totalSeconds =Time.getTotalSeconds("01:01:01");		
//		assertTrue("The time provided does not match the result", totalSeconds == 3661);
//		
//	}
//	@Test (expected = NumberFormatException.class)
//	public void testGetTotalSecondsException() {
//		int totalSeconds =Time.getTotalSeconds("01:01:a1");		
//		assertTrue("The time provided does not match the result", totalSeconds == 3661);
//		
//	}
//	@Test
//	public void testGetTotalSecondsBoundaryIn() {
//		int totalSeconds =Time.getTotalSeconds("01:01:59");		
//		assertTrue("The time provided does not match the result", totalSeconds == 3719);
//		
//	}
//	@Test (expected = NumberFormatException.class)
//	public void testGetTotalSecondsBoundaryOut() {
//		int totalSeconds =Time.getTotalSeconds("01:01:60");		
//		assertTrue("The time provided does not match the result", totalSeconds == 3720);
//		
//	}
	@Test
	public void testGetTotalMillisecondsRegular() {
		int totalSeconds =Time.getTotalMilliseconds("12:05:50:05");		
		assertTrue("Invalid number of miliseconds", totalSeconds == 5 );
		
	}
	@Test(expected= NumberFormatException.class)
	public void testGetTotalMillisecondsException() {
		int totalSeconds =Time.getTotalMilliseconds("12:05:50:0A");		
		assertFalse("Invalid number of miliseconds", totalSeconds == 5 );
		
	}
	@Test
	public void testGetTotalMillisecondsBoundaryIn() {
		int totalSeconds =Time.getTotalMilliseconds("12:05:50:999");		
		assertTrue("Invalid number of miliseconds", totalSeconds == 999 );
		
	}
	@Test(expected= NumberFormatException.class)
	public void testGetTotalMillisecondsBoundaryOut() {
		int totalSeconds =Time.getTotalMilliseconds("12:05:50:1000");		
		assertTrue("Invalid number of miliseconds", totalSeconds == 999 );
		
	}
	
}

// Made by Vinayak Pavate