package password;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import org.junit.Test;

/*
 * @Author Vinayak Pavate
 * Student ID: 991548217
 * Assumption is that spaces are not considered valid chracters
 */
public class PasswordValidatorTest {

//	@Test
//	public void testHasEnoughDigitsRegular() {
//	assertTrue( "Invalid number of digits in password",PasswordValidator.hasEnoughDigits("ABC12"));
//	}
//	@Test
//	public void testHasEnoughDigitsException() {
//	assertFalse( "Invalid length in password",PasswordValidator.hasEnoughDigits("ABDE1"));
//	}
//
//	@Test
//	public void testHasEnoughDigitsBoundaryIn() {
//	assertTrue( "Invalid length in password",PasswordValidator.hasEnoughDigits("ABCD12334"));
//
//	}
//	@Test
//	public void testHasEnoughDigitsBoundaryOut() {
//	assertFalse( "Invalid length in password",PasswordValidator.hasEnoughDigits("ABFD1"));
//
//	}
 @Test
 public void testIsValidLengthRegular() {
	 assertTrue( "Invalid length of password",PasswordValidator.isValidLength("1234567890"));
	}
 @Test
	public void testIsValidLengthException() {
	assertFalse( "Invalid length in password",PasswordValidator.isValidLength(null));
	}
 @Test
	public void testIsValidLengthBoundaryIn() {
	assertTrue( "Invalid length in password",PasswordValidator.isValidLength("12345678"));

	}
	@Test
	public void testIsValidLengthBoundaryOut() {
	assertFalse( "Invalid length in password",PasswordValidator.isValidLength("1234567"));

	}
}

